package Account;

public class Account {
    public String id;
    public String name;
    public int balance;
    public Account(String id, String name, int balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }
    public Account(String id, String name) {
        this.id = id;
        this.name = name;
    }
    public String getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public int getBalance() {
        return balance;
    }

    public int credit(int amount){
        return balance = balance + amount;
    }

    public int debit(int amount){
        if(amount <= balance){
            return balance = balance - amount;
        }else{
            System.out.println("Amount exceeded balance");
            return balance;
        }
    }

    public int transferTo(Account another, int amount){
        if(amount <= balance){
            return another.balance = another.balance + amount;
        }else {
            System.out.println("Amount exceeded balance");
            return balance;
        }
    }
    @Override
    public String toString() {
        return "Account [id=" + id + ", name=" + name + ", balance=" + balance + "]";
    }


}
